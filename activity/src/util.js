const names = {
    "Brandon": {
        "name": "Brandon Boyd",
        "alias": "Brandy",
        "age": 35,
    },
    "Steve": {
        "name": "Style Tyler",
        "alias": "Stevie",
        "age": 56,
    }
}

module.exports = {
    names: names
}